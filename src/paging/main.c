/************************************/
/*			SYS INCLUDES			*/
/************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>


/************************************/
/*			LOCAL INCLUDES			*/
/************************************/
#include "src/MMU.h"


/************************************/
/*			FUNCTION DEFS			*/
/************************************/
void usage( char * argv[] );


int main( int32_t argc, char * argv[] )
{
	if( argc != 3 )
	{
		usage(argv);
		return EXIT_FAILURE;
	}

	printf("Hello Paging \n");
	return EXIT_SUCCESS;
}

inline void usage( char * argv[] )
{
	fprintf(stderr, "Usage: %s [PageSize] [PhysicalAddressSpace]", argv[0]);
	return;
}
