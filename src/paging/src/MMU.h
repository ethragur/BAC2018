#pragma once


#include <stdint.h>
#include <stddef.h>


uint32_t * get_physical_address( uint32_t * logical_address );
