/************************************/
/*			SYS INCLUDES			*/
/************************************/
#include <iostream>
#include <vector>
#include <cstdint>


/************************************/
/*			LOCAL INCLUDES			*/
/************************************/
#include "src/MMU.hpp"
#include "src/process.hpp"


/************************************/
/*			FUNCTION DEFS			*/
/************************************/
inline void usage( std::string p_name );



int main( int argc, char * argv[] )
{
//	if( argc != 3 )
//	{
//		usage(argv[0]);
//		return EXIT_FAILURE;
//	}


	//reserve that much space for page_tables
	auto memory_unit = MMU(1024*1024, 4);


//	memory_unit.split_address(0x00801004);
	printf("Proc1: %d\n", memory_unit.register_proc());
	printf("Proc2: %d\n", memory_unit.register_proc());
	printf("Proc2: %d\n", memory_unit.register_proc());
	printf("Proc2: %d\n", memory_unit.register_proc());



	memory_unit.map_page(0x00c004, 0x00801004);

	printf("physical address: %#08x\n", memory_unit.get_phys_address(0x00801004));


//	auto process = Process("test", 8);

	return 1;
}

inline void usage( std::string p_name )
{
	std::cerr << "Usage: " << p_name << " [numberOfProcs] []" << std::endl;
	return;
}
