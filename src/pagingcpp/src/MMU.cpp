
#include "MMU.hpp"

#include <iostream>


/**************************************************************************
 * Returns a physical address from a virtualaddress,
 * First all the offsets from the virtual address are calculated
 * The Virtual Address has the following structure:
 * |  PageDirOffset(10bit) | PageTableOffset(10bit) | PageOffset(12bit)
 *
 * After that the we calculate the pagedirectory by using the cr3 from the current
 * process + pagedir offset. This gives us the address of the directory, if the present
 * flag is not set we return (TODO: page fault)
 *
 * Next step is the same thing with the pagetable, we calculate the pagedir start from the pagedirectory
 * | PageTable(20bit) | Flags (10bit)
 * We add that to the pagetable offset from our vitualaddress to get our page table entry
 * Finally We do the same thing for the acutal page (there we need no memory access, we
 * already know the address inside the page table and the offset)
 ************************************************************************/ 
int32_t MMU::get_phys_address( int32_t virtualaddress)
{
	printf("Getting physical address from virtual address: %#08x \n", virtualaddress);
	//only use the first 12 bit (0xFFF)
	int32_t page_offset    =  virtualaddress & 0x0FFF;
	//Move address 12 bit to the right and only care about the first 10 bit
	int32_t pagetable_offset =  virtualaddress >> 12 & 0x03FF;
	// remove the everything except the last 10 bit
	int32_t pagedir_offset   =  virtualaddress >> 22 ;


	//mem is where the page structures are stored, p_cr3 is the PD start entry for the current Process
	//pagedir_offset is the calculated offset (first 10 bit) given virtual address
	//The result is the pagedirectory we need
	int32_t pagedir = *(mem+this->p_cr3+pagedir_offset) ;

	if(!this->is_present(pagedir))
	{
		return 0;
	}


	//we need bit 31-11 for the address of the pagetable inside the page directory, after that fill up 4K
	//TODO: could also be done with & operator
	int32_t pagetable_address = pagedir >> 12 << 12; 

	//finally we get the currently needed page entry
	int32_t pagetable = *(mem+pagetable_address+pagetable_offset);

	if(!this->is_present(pagetable))
	{
		return 0;
	}

	// Same as above with the table, we need the 20 bits from the page table
	int32_t page_address = pagetable >> 12 << 12;

	int32_t page = page_address + page_offset;


	return page;
}


/**************************************************************************
 * Maps a page from a physical address to a virtual
 * Quite similiar to function above
 * gets a free page table entry if page doent exist
 *************************************************************************/
void	MMU::map_page (int32_t physaddress, int32_t virtualaddress )
{
	printf("Mapping page from phys: %#08x to virtual address: %#08x \n" ,physaddress, virtualaddress);
	int32_t pagetable_offset =  virtualaddress >> 12 & 0x03FF;
	// remove the everything except the last 10 bit
	int32_t pagedir_offset   =  virtualaddress >> 22 ;

	int32_t pagedir = *(mem+this->p_cr3+pagedir_offset) ;


	if(!this->is_present(pagedir))
	{
		//if the  the pagedirectory is not present we have to create a page table
		// set present flag
		int32_t new_pagetable_addr = get_free_memory();
		*(mem+this->p_cr3+pagedir_offset) = new_pagetable_addr | 0x1 ;

	}
	pagedir = *(mem+this->p_cr3+pagedir_offset) ;

	//we need bit 31-11 for the address of the pagetable inside the page directory, after that fill up 4K
	//TODO: could also be done with & operator
	int32_t pagetable_address = pagedir & 0xFFFFF000;
	//int32_t pagetable_address = pagedir >> 12 << 12; 

	//finally we get the currently needed page entry
	int32_t pagetable = *(mem+pagetable_address+pagetable_offset);

	if(this->is_present(pagetable))
	{
		//TODO
		//page is already mapped what do to know???
		//remap or nothing
	}
	
	//Page align phys address and set present flag
	*(mem+pagetable_address+pagetable_offset) = (physaddress & 0xFFFFF000) | 0x001;
}


int32_t  MMU::split_address ( int32_t virtualaddress)
{
	//only use the first 12 bit (0xFFF)
	int32_t offset    =  virtualaddress & 0x0FFF;
	//Move address 12 bit to the right and only care about the first 10 bit
	int32_t pagetable =  virtualaddress >> 12 & 0x03FF;
	// remove the everything except the last 10 bit
	int32_t pagedir   =  virtualaddress >> 22 ;

	printf("pagedir: %#08x\n", pagedir);
	printf("pagetable: %#08x\n", pagetable);
	printf("pageoffset: %#08x\n", offset);

	return 0;
}


/**********************************************
 * Registers a process by initializing the 2nd bit
 **********************************************/
int32_t MMU::register_proc( )
{
	//TODO make the number of PD a variable
	for(int i = 0; i < n_proc*1024; i+=1024)
	{
		if(*(mem+i) >> 1 & 1)
		{
			continue;
		}
		else
		{
			for(int j = 0; j < 1024; j++)
			{
				*(mem+i+j) |= 0x2;
			}
			return i;
		}
	}

	return 0;
}

int32_t MMU::get_free_memory ( )
{
	for(int i = 1024 * n_proc; i < 1024*1024*n_proc; i++)
	{
		if( !is_present(*(mem+i)) && !(*(mem+i) >> 1 & 1))
		{
				*(mem+i) |= 0x2;
				return i;
		}
	}
	return 0;
}



