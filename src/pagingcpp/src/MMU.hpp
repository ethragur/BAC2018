#pragma once

#include <cstdint>
#include <iostream>


class MMU
{
	public:
		//CR3 register of current proc
		int32_t p_cr3;
		//memory space for page structures (would be kernel memory)
		int32_t * mem;
		//maximum number of processes
		int32_t n_proc;
		
	
		int32_t get_phys_address( int32_t virtualaddress);
		int32_t split_address	( int32_t virtualaddress);
		void	map_page		( int32_t physaddress, int32_t virtualaddress );
		int32_t register_proc   ( );


		MMU (int32_t size, int32_t _n_proc) : n_proc(_n_proc)
		{
			mem = new int32_t[size * n_proc];
			p_cr3 = 0;


		}

		~MMU()
		{
			delete [] mem;
		}

	private:
		int32_t is_present( int32_t virtualaddress )
		{
			//when the first bit of a virtualaddress is 1 it is present
			return virtualaddress & 0x1;
		}
		int32_t get_free_memory ( );
};
