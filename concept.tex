\documentclass{article}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{float}
\usepackage{listings}
\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue}
}

\begin{document}

\title{Paging Concept on x86\_64}
\author{Stefan Kuhnert}

\maketitle

\begin{abstract}
    BAC-Thesis concept for algorithms, architecture implementations and visualization of page replacement strategies
\end{abstract}

\section{Introduction}
In this thesis I will try to cover the basic concepts of Page replacement strategies in modern architectures and operating system. 
First I will introduce the theory of Paging on x86\_64 (AMD64) which is used in most modern desktop and server infrastructure and
explain the differences and limitations to its predecessor x86 (i386).
\newline
\newline
The next chapter will cover the basic replacement strategies (FIFO, LRU, \ldots) and their advanced algorithms which are currently in use in the Linux and FreeBSD kernel.
\newline
\newline
The last part of my thesis will cover theories and implementations of said algorithms as well as ways to visualize those for learning and presentation purposes.
This will done in cooperation with Robert Shaffenrath and his Thesis: services and visualization in operating systems.

\subsection{Resources}

For the x86\_64 Design i will use \href{https://wiki.osdev.org/Paging}{wiki.osdev.com}. It is a great learning resource for the hardware fundamentals of operating systems.

The \href{https://0xax.gitbooks.io/linux-insides/content/}{linux-insides} book is a learning resource written by a linux kernel developer. It explains the insides of the kernel step by step and provides simple and clean code examples of algorithms in use.

For deeper theory the \href{https://www.kernel.org/doc/gorman/html/understand/understand013.html}{official kernel documentation} on page replacement can be used as well as the actual \href{https://git.kernel.org/}{implementation of the linux kernel}. 
Different implementations can be seen in the FreeBSD operating system. Literature: \href{https://www.amazon.com/Design-Implementation-FreeBSD-Operating-System/dp/0321968972}{The Design and Implementation of the FreeBSD Operating System (2^{nd} Edition)}.




\section{Paging Design in the x86\_64 Architecture}

The main part of this chapter is to explain the theory and fundamentals of memory management. Most tutorials and learning material cover only the 32bit counterpart. 

\subsection{Basic Paging}

The basic idea of paging is a system which allows each process to see a full virtual address space, without actually requiring the full amount of physical memory to be available or present.
This is done by mapping all the available system memory into pages. The process of translating access to those pages is done by the memory management unit (MMU). 

One of the major benefits of this technique of accessing memory is the advanced security gain due to process only having access to its assigned pages. But as seen in the recent Intel Meltdown Bug, CPU manufacture optimize paging and memory access and therefore introduce new memory vulnerabilities.

\subsection{Differences between x86\_64 and and x86}

\subsubsection{Segmentation}
Segmentation was introduced in 1978 with the Intel 8086. It was a way to split the memory up into segments that can be of size 1 byte up to 64KB\@. The x86 CPU has special registers  (CS, DS, SS and ES) in order to calculate memory offset addresses.  
\newline
\newline
With the Introduction of x86\_64 the segmentation base addresses were all set to 0. That means that all segments are overlapping and are occupying all the linear address space (Logical address == Linear Address).
The only part of segmentation still used in Long Mode (64 Bit Mode) is the CS (Code Segment Register). By reading its value the processor can assume the privilege level of the running code.
Also AMD has enabled limited support on its x86\_64 CPU's due to ease of virtualization.

\subsubsection{Physical Address Extension}

On of the major downsides with paging in x86 was the limitation of the virtual address space (4GB). 
In order to address the increased memory demand the Physical Address Extension(PAE) was introduced into the x86 Architecture.
PAE is a special flag in modern x86 Processors. It increased the size of a table to 64 bit instead of 32 bit by creating a new Data Structure(Page Directory Pointer Table) (Even though most CPU's had a hard limit of memory access with 36bit).

While the whole paging table could now access up to 64GiB of memory \((2^{36})\) a single process was never able to see more than 4GiB.

The idea behind PAE was used for the paging design in x86\_64.


\subsection{Multilevel Paging}

32bit processors used a 2-Level Paging Architecture. It contained two tables: the Page Directory and the Page Table. Both are arrays that contain 1024 entries. The Page Directory references Page Tables and the Page Tables references physical Memory location.
In x86\_64 this system was extended to a multilevel architecture. Instead of two structure there can be an arbitrary number of directory structures, for as long as they can be accessed by one word (64Bit).
Most modern operating systems and CPU's use a 4-Level paging structure right now.

\newline
\begin{figure}[h!]
    \includegraphics[width=\textwidth]{VirtualAddressPaging.png}
    \centering
    \caption{Virtual layout of a 4-Level x86\_64 address}
    \label{fig:virtaddr}
\end{figure}


\subsubsection{Sidenote: MAXPHYADDR}

A word on the x86\_64 Architecture is 64 bit long. Normally this value is not used for address referencing because it would make the wholse system more complex (\(2^{64} = 18.4 \) Exabytes). 
Therefore most modern CPUs have this size reduced. This phyiscal-address width is refered to as "MAXPHYADDR" and is in general 36bit.

The maximum size of MAXPHYADDR is 52 bits (the other 12 bits are used for paging internal flags as mentiond in section \ref{direntry}).

In order to find out the MAXPHYADDR of the CPU you can read the CPUID at location CPUID.80000008H:EAX[7:0]. 
\subsubsection{\%CR3 Register}

The Control Register 3 is a special register in the x86 and x86\_64 that is used as a starting point for the linear address translation.
It contains the address of the highest paging directory for the process that is accessing memory. 

On the x84\_64 Architecture it contains 48bit to reference the highest level paging directory and only has two flags on bit 4 and 3.

\begin{itemize}
        \item PCD(4): Page-level cache disable
        \item PWT(3): Page-level write-through
\end{itemize}

\subsubsection{Directory Entry} \label{direntry}


The Structure of each directory/table is quite similar, referencing its lower level directory or in case of the page table the physical memory location.

\newline
\begin{figure}[h!]
    \includegraphics[width=\textwidth]{PageDirectoryStructure.png}
    \centering
    \caption{General hierarchical paging structure}
    \label{fig:pagestr}
\end{figure}

\begin{itemize}
        \item \textbf{NX:} No Execute, forbids execution of code on this page
        \item \textbf{Reserved:} Can be freely used by OS. Might be used for future levels of paging
        \item \textbf{Addr:} The address of the lower level paging structure or to the page itself
        \item \textbf{Available:} Available bits for the operating system (e.g. swapping information)
        \item \textbf{G:} Global, should be 0 except when Paging Global Extension(PGE) is turned on (TODO: footnote PGE)
        \item \textbf{PS:} Page Size, must be 0 when referencing a page table, if it is set to 1 it references a 2MB Page
        \item \textbf{D:} Dirty Flag, is set when software has written to the Page referenced by the table. Ignored on page directories 
        \item \textbf{A:} Access Flag, indicates if the directory has been used for address translation, or the referenced page has been accessed.
        \item \textbf{PCD:} Page-level cache disable, disables caching for the reference
        \item \textbf{PWT:} Page-level write-through, writes go directly to memory
        \item \textbf{US:} User/Supervisor, if set everyone can access the page, otherwise ring 0 is allowed
        \item \textbf{WR:} Read/Write, if 0 only reads are allowed on the page
        \item \textbf{P:} Present, if set the page is in memory and can be used
\end{itemize}


\subsubsection{Linear Adress Translation}

In order to get the phyiscal address of a memory access, the operating system has to perform a translation of the page hierarchy.
For this translation a special structure is used (Paging Structure). The Top-Level Paging structure (Page Global Directory) is then loaded into the \%cr3 register.

Then the address translation is as following:

\begin{itemize}
  \item The given address arrives at the MMU (Memory Management Unit)
  \item The 64-bit linear address is split into 4 Parts. Only the low 48 bits are significant (as seen in Figure \ref{fig:virtaddr})
  \item The \%cr3 register stores the address of the 4 top-level paging structure
  \item 47:39 bits of the given linear address store an index into the paging structure level-4, 38:30 bits store index into the paging structure level-3, 29:21 bits store an index into the paging structure level-2, 20:12 bits store an index into the paging structure level-1 and 11:0 bits provide the offset into the physical page in byte.
\end{itemize}

\begin{figure}[H]
    \includegraphics[width=\textwidth]{LinearAddressTranslation.png}
    \centering
    \caption{Schematics of the Translation Process}
    \label{fig:pageaddrtrans}
\end{figure}


\newline


\subsection{Page Fault Exception}

Paging allows the operating system to reference more virtual memory then actual available physical memory. Since the kernel memory and user-land memory are separated the operating system is still able to function even though no memory is available.
If this case arises, the operating system only has two choices left:
\begin{itemize}
        \item Kill random processes in order to free up memory
        \item Prohibit allocation of memory and hope that process free up memory for themselves
\end{itemize}
Both possibilities are not optimal for the stability of the system.
In order to avoid this catastrophic outcome the operating system can store pages to a lower memory tier.
Most of the time the lower memory tier is persistent memory (HDD, SSD, ...) which is multiple times slower than the systems dynamic random-access-memory (DRAM).
The process of writing pages to physical storage devices is called swapping. The swap space (the place where the pages persist on the storage) can either be a file (swap file) or a separate swap partition.
In general the advantages of a swap partition is the better performance in contrast to a file, due to the fact that the file access is handled by a file system while the partition can manage access on its own. The advantage of a swap file is that it can be created or changed afterwards without too much of a hassle.
Due to slow read and write times on physical storage devices the operating system has to minimize those accesses.

The trigger of a page replacement is a so called "page fault exception".

\subsubsection{Types of Page Faults}

The page fault exception is an exception raised by the hardware when a running program access a memory page that is not currently mapped by the MMU.
There are 3 different kinds of Page Faults: Minor, Major and Invalid.

\begin{itemize}
	\item Minor Page Fault

This exception is raised when the requested page is already in memory but was not assigned to the process which was accessing it.
A typical scenario where this happens is when two processes share memory with each other (Process B wants to access a page from process A. While the page is already in memory it hasn't been assigned to process B so a minor page fault is raised).

\item Major Page Fault

This type is also referred as hard page fault. It is raised when the requested page isn't in memory and has to be loaded from disk.
If free memory is available the process is rather simple, since the page can be directly loaded into memory.
If no memory is available the OS has to use certain strategies in order to free up memory for the requested page (this process is called page replacement, and certain strategies will be discussed further down the concept).

\item Invalid

This fault is raised when the accessed page doesn't exist. Most of the time this is caused by a software bug, most commonly by dereferencing a NULL pointer.
In UNIX and UNIX-Like systems this causes a so called Segmentation Fault or Bus Error (SIGSEGV). If the program isn't handling this Signal, the OS will terminate it. 

\end{itemize}

\newline
On UNIX or UNIX-Like Systems you can see the number of minor and major page faults with the process command (ps)
\newline

\begin{lstlisting}[language=bash, caption={Minor and Major Page Faults per process}]

#!/usr/bin/bash

ps -eo min_flt,maj_flt,cmd
\end{lstlisting}

This command outputs a list of all process with the number of minor and major page faults. On a system that has a lot of free memory available the number of major page faults should be rather low.
Another UNIX command that shows interesting memory information is "time"

\begin{lstlisting}[language=bash, caption={System information of a process}]

#!/usr/bin/bash

time -v firefox
\end{lstlisting}

This will launch firefox and after exiting it will show a few informations about the process 
E.g.
\begin{lstlisting}[language=bash, caption={Result of time -v }]
User time (seconds): 1.36
System time (seconds): 0.47
Percent of CPU this job got: 70%
Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.62
Average shared text size (kbytes): 0
Average unshared data size (kbytes): 0
Average stack size (kbytes): 0
Average total size (kbytes): 0
Maximum resident set size (kbytes): 196156
Average resident set size (kbytes): 0
Major (requiring I/O) page faults: 616
Minor (reclaiming a frame) page faults: 161116
Voluntary context switches: 10823
Involuntary context switches: 1226
Swaps: 0
File system inputs: 214744
File system outputs: 3872
Socket messages sent: 0
Socket messages received: 0
Signals delivered: 0
Page size (bytes): 4096
Exit status: 0
\end{lstlisting}

Interesting numbers here are the Major and Minor page faults which are explained above.
Swaps describest the number of times this process was swapped out of main memory.


\subsection{Page Replacement Strategies}

When a major page fault exception was raised an there is no free memory available, strategies have to be deployed in order to avoid a total system shutdown.
Since reading and writing to I/O is a rather expensive task it is very important for those algorithms to minimize the wait time for each page replacement but also minimize the number of page replacements in the future.

\subsubsection{FIFO - First in First out}

This is the simplest page replacement algorithm. It tracks all pages in a queue, with the oldest page in front other queue.
When a new page is loaded in and no free space is available it removes its head.

\subsubsection{LRU - Least Recently used}


\subsubsection{Clock Algorithm}
\subsubsection{Optimal Strategy}
\subsubsection{SPLIT LRU - Linux Page Replacement Algorithm}
\subsubsection{Free Page Determination Algorithm - FreeBSD}

\subsection{Examples Exercises}

\subsubsection{ 1. Working Set }

Variables:

\begin{itemize}
  \item Windows Size(\(\Delta\)):  The maximum number of pages in memory for each process, is chosen by the operating system
    \item Time of Last Use(t): The Time the page was last used
    \item Referenced Bit (R): Has the page been accessed?
    \item Time Window (T): All pages refrenced in the last T seconds comprise the working set
\end{itemize}

Alogrithm:
A periodick clock interrupt clears each Referenced Bit on every tick in a time interval of T. On every page fault the page table is scanned to look for a suitable page to evict.

The age of a page is the current time minus the time of last use (t).

If R == 1 the current time is written into the Time of Last Use field of the Page. Since age < T the page is not a candiate for removal.

If R == 0 (the page was not used in the last tick) 2 cases have to be considered:

Case age > T:

    The Time of Last Use is bigger than our Time Window. This means the page is old and can be removed

Case age < T:

    The page is temporarily spared, but the algorithm remembers the oldest page. If no other page is found where age > T the algorithm will remove the page with the oldest age.

\newpage

Solution of 1. With Window Size 2:

\begin{center}
\begin{tabular}{ |c||c|c| }
  \hline
  Refs & \Delta=2 \& T = 2 & \Delta=2 \& T = 4:\\ 
  \hline
 1 & 1   & 1  \\ 
 2 & 1 2 & 1 2\\ 
 3 & 2 3 & 2 3\\ 
 4 & 3 4 & 3 4\\ 
 5 & 4 5 & 4 5\\ 
 2 & 5 2 & 5 2\\ 
 1 & 2 1 & 2 1\\ 
 3 & 1 3 & 1 3\\ 
 3 & 3   & 1 3\\ 
 2 & 3 2 & 3 2\\ 
 3 & 2 3 & 2 3\\ 
 \hline
\end{tabular}
\end{center}

With T = 4 there is on difference: the 1 in the 3rd from the last row wasn't removed from the working set because it was accessed 4 Time Units ago.


\subsubsection{ 2. Page Replacement Strategies }

Comments: 

Specifient the virtual page and the page frame is confusing at this exercise, since they provide no extra information. The exercise would be the same with just the page id given.

What is the difference between referenced and modified bit (in all Implementation they are the same, OS does not care if it was read or write)
Or is it the second chance algorithm (bits are called used and dirty)

\begin{itemize}
\item a. FIFO 
  Page Frame 3 will be replaced because it was loaded in first in the Page Table
\item b. LRU
  Page Frame 1 will be replaced because it was last accessed
\item c. Clock Algorithm 
  Under the assumption the clock algorith is used, not the second chance Page Frame 0 (page 2) will be replaced.
  The pointer of the clock will start at page frame 2 (whose reference bit is set to 1) set R to 0 and it does the same.
  One time around the clock it will land 
  
\begin{figure}[H]
    \includegraphics[width=50mm]{ClockStragie.png}
    \centering
    \caption{Clock Algorithm}
    \label{fig:clockalgo}
\end{figure}

\item d. Optimal Strategy
  With the reference sequence of virtual pages ('4,0,0,2,4,2,1,0,3,2') the  virtual page 3 will be replaced because its page access occurs farthest in the future

\item e. Working Set
  (No Window Size given, will take 4 same as above). 
    Starting Working set would be:
\end{itemize}

\begin{center}
\begin{tabular}{ |c||c|c| }
  \hline
  s & 3 0 2 1 & Start \\ 
  4 & 0 2 1 4 & pagefault\\ 
  0 & 0 2 1 4 & -\\ 
  0 & 0 2 1 4 & -\\ 
  0 & 0 2 1 4 & -\\ 
  2 & 0 2 1 4 & -\\ 
  4 & 0 2 1 4 & -\\ 
  2 & 0 2 1 4 & -\\ 
  1 & 0 2 1 4 & -\\ 
  0 & 0 2 1 4 & -\\ 
  3 & 3 2 1 4 & pagefault\\ 
  2 & 3 2 1 4 & - \\ 
 \hline
\end{tabular}
\end{center}

\newpage
\subsubsection{ 3. Address Space }

\begin{itemize}
  \item Page Size = 2K
  \item Physical Address Space = 1MB
  \item Physical Address = 20bit \\((log_2(1MB)\)
  \item Nr. of Pages = 32
  \item Logical Address Space = Number of Pages * Page Size = 64KB
  \item Logical Address = 16bit = \(log_2(64KB)\)
  \item Nr. of Frames = Physical Address Space / Page Size = 512
\end{itemize}

\newline
Solution: 
\begin{itemize}
  \item a. Logical Address Size = 16 bit, in order to reference the number of pages (32) 5 bit will be needed. 
  \item b. We need 9 bit to reference 512 Frames \(log_2(512)\)
  \item c. When Physical Address Space = 512KB \rightarrow Frame Size = 256. \(log_2(256)\) = 8bit
\end{itemize}


\subsubsection{ 4. Memory Management }
a)
\begin{itemize}
  \item Logical Address = 48bit
  \item Physical Address = 32bit
  \item Page Size = 8KB
  \item \(log_2(8KB) = 13bit
  \item #Pages = 2^{48} / 2^{13} = 2^{35} 

\end{itemize}

b)
Free gaps in memory: 10KB, 4KB, 20KB, 12KB, 9KB, 12KB, 15KB
\newline
To allocate: 10KB, 12KB, 8KB

\begin{itemize}
  \item First Fit: 
    Allocate the requested memory in the first free gap that fits \newline
    4KB, 12KB, 9KB, 12KB, 15KB
  \item Best  Fit: 
    Allocate the requested memory with the best fitting (size is the closest) gap \newline
    4KB, 20KB, 1KB, 12KB, 15KB
  \item Worst Fit: 
    Allways split the biggest memory block and allocate the requestd memory in there \newline
    10KB, 4KB, 10KB, 4KB, 9KB, 12KB, 3KB
\end{itemize}





\subsection{Visualization of Paging Replacement Algorithms}

\section{Implementation of Paging Strategies}
\subsection{Memory Allocation Strategies}
These types of algorithms provide new page frames when the application needs it (memory allocation). 

\subsubsection{Stack/Linked List}
Simple data structure where the address of each physical frame is stored in a stack structure. Provides fast allocation but slow free or check speeds.
\subsubsection{Bitmap}
See Implementation. Array access of byte level
\subsubsection{Buddy System}
The memory allocation system used by the linux kernel.
\end{document}
